package taskb;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jakub
 */
public class CircleTest {
    
    /**
     * Test of getArea method, of class Circle.
     */
    @Test
    public void testGetArea() {
        Circle instance = new Circle(25.0, "red");
        assertEquals(1963.4954, instance.getArea(), 0.015);
        System.out.println(instance.getArea());
    }
    @Test
    public void testGetShapeColour() {
        Circle instanceNew = new Circle(25.0, "green");
        System.out.println("Circle colour is" + instanceNew.getShapeColour());
        assertEquals("green", instanceNew.getShapeColour()); // test fails - shapeColour isNull ???
        System.out.println(instanceNew.getShapeColour());
    }
    
}

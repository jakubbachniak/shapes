package taskb;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class TriangleTest {
    /**
     * Test of getArea method, of class Triangle.
     */
    @Test
    public void testGetArea() {
        Triangle instance = new Triangle("Green", 5.0, 7.0);
        assertEquals(17.5, instance.getArea(), 0.01);
        System.out.println(instance.getArea());
    }
    
}

package taskb;

/**
 *
 * @author Jakub
 */
    public class Rectangle extends AbstractShape  implements Shape {
    
    private double width;
    private double height;
    
    // default no-args constructor
    public Rectangle() {
        
    }
    
    // create an instance of Rectangle
    public Rectangle(String shapeColour, double width, double height){
        this.shapeColour = shapeColour;
        this.width = width;
        this.height = height;
    }
    
    // mutator methods
    
    // accessor methods

    public double getHeight() {
        return height;
    }
    public double getWidth() {
        return width;
    }
    public double getArea() {
        return width * height  ;
    }
    public void draw () {
        System.out.println("This is a rectangle and its colour is " + shapeColour);
    }
}

package taskb;

/**
 *
 * @author Jakub
 */
public class TaskB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Triangle newTriangle = new Triangle("Pink", 12.0, 15.0);
        //System.out.println(newTriangle.toString());
        //Triangle newTriangle2 = new Triangle("zielony");
        //Circle newCircle = new Circle(5.0, "czerwony");
        //System.out.println("the circle has colour " + newCircle.getShapeColour());
        
        //Simple try of polymorphism
        Shape newShape = new Triangle("Purple", 5.0, 7.0);
        System.out.println("The area of the triangle is " + newShape.getArea());
        
        // 
        Circle newCircle = new Circle(25.0, "Purple");
        System.out.println("The colour of the new circle object is " + 
                            newCircle.getShapeColour()); // here the getShapeColour yields null... huh???
    }
    
}

package taskb;

import java.lang.Math;

/**
 *
 * @author Jakub
 */
public class Circle extends AbstractShape implements Shape {
    
    // fields
    private double radius;
    
    // default no-args constructor
    public Circle() {
    }
    
    // create Circle
    public Circle(double radius, String shapeColour) {
        this.radius = radius;
        this.shapeColour = shapeColour;
    }
    
    // mutator methods
    public void setRadius(int r) {
        radius = r;
    }
    
    // accessor methods
    public double getRadius() {
        return radius;
    }
    public double getArea() {
        return Math.PI*Math.pow(radius, 2.0);
    }
}

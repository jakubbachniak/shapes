package taskb;

interface Shape {
   
    
    // mutator methods
    void setShapeColour(String shapeColour);
    void draw ();
    
    // accessor methods

    String getShapeColour();
    double getArea();
    
}

package taskb;


/**
 *
 * @author Jakub
 */
public abstract class AbstractShape implements Shape{
    protected String shapeColour;
    
    @Override
    public void setShapeColour(String shapeColour) {
        this.shapeColour = shapeColour;
    }
    @Override
    public String getShapeColour(){
        return shapeColour;
    }
    @Override
    public void draw() {
        System.out.println( "This is a " + this.getClass().getName() +
                            "and its colour is " + this.shapeColour);
    }
    
}

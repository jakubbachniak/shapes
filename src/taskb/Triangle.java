package taskb;

/**
 *
 * @author Jakub
 */
public class Triangle extends AbstractShape implements Shape {
    
    private double base;
    private double height;
    
    // default no-args constructor
    public Triangle(){
        
    }
    // create an instance of Triangle
    public Triangle(String shapeColour, double base, double height) {
        this.shapeColour = shapeColour;
        this.base = base;
        this.height = height;
    }
    // mutator methods
    public void setBase(double base) {
        this.base = base;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    
    // accessor methods
    public double getBae() {
        return base;
    }
    public double getHeight() {
        return height;
    }
    public double getArea(){
        return (base*height)/2;
    }
}
    
